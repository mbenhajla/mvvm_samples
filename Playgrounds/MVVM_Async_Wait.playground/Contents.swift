import UIKit
import Foundation
import SwiftUI
import Combine
import PlaygroundSupport

// Service and Repo


protocol RepositoryProtocol{
    
    func loadImage() -> AnyPublisher<UIImage, Never>
    
}

struct RepositoryService:RepositoryProtocol{
    func loadImage() -> AnyPublisher<UIImage, Never> {
        let url:URL = URL(string: "https://smartskills.io/assets_apps/Blockchain_ChOpen_workshop2.png")!
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data)! }
            .replaceError(with: UIImage())
            .eraseToAnyPublisher()
        
    }
    
    
    
    func loadImageAsyncWait() async -> UIImage {
        let url:URL = URL(string: "https://smartskills.io/assets_apps/Blockchain_conf_talk_berlin.png")!
        
        do{
            let (data, _) = try await URLSession.shared.data(from:  url)
            let image = UIImage(data: data)!
            return image
            
            
        }catch{
            print ("Error")
            
        }
        return UIImage()
    }
    
    
    
    func loadImageWithFuture() ->  Future<UIImage, Never>{
        
        return Future(asyncFunc: {
                await loadImageAsyncWait()
            })
        
    }
    
}


// Extension that allows us to convert an async func to a Future publisher. Wwe now have the possibility to use it on any async function to bridge it to Combine's Future type.

extension Future where Failure == Error {
   convenience init(asyncFunc: @escaping () async throws -> Output) {
       self.init { promise in
           Task {
               do {
                   let result = try await asyncFunc()
                   promise(.success(result))
               } catch {
                   promise(.failure(error))
               }
           }
       }
   }
}

//For non-throwing async functions, we  add another extension:

extension Future where Failure == Never {

    convenience init(asyncFunc: @escaping () async -> Output) {
        self.init { promise in
            Task {
                let result = await asyncFunc()
                promise(.success(result))
            }
        }
    }
}


// ViewModel

class CounterViewModel:ObservableObject {
    @Published var counter:Int = 1
    @Published private(set) var image: UIImage = UIImage()
    private var subscription: AnyCancellable?
    
    func increment() {
        counter += 1
    }
    
    func loadImage(){
        subscription = RepositoryService()
            .loadImage()
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
    }
    
    func loadImageWithFuture() {
        subscription = RepositoryService()
            .loadImageWithFuture()
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
        
    }
    
    
    func loadImageAsync() async{
        
        image = await RepositoryService()
            .loadImageAsyncWait()
    }
    
    func cancel() {
        subscription?.cancel()
    }
}







// View

struct CounterView:View{
    @ObservedObject var model:CounterViewModel
    
    var body:some View{
        
        Text("Counter")
        Text("\(model.counter)")
        VStack {
            // 3
            
            Image(uiImage: model.image)
                .renderingMode(.original)
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
            
            
            
        }
        .onAppear {   }
        .onDisappear { self.model.cancel() }
        
        Button("+", action: { self.model.increment()})
        
        Button("Load", action: { self.model.loadImage()})
        
        Button("Load Async ", action: {
            // Check also https://www.swiftbysundell.com/articles/building-an-async-swiftui-button/
            
            Task {
                await self.model.loadImageAsync()
            }
        })
        
        VStack {
            // 3
            
            Image(uiImage: model.image)
                .renderingMode(.original)
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)
            
            
            
        }
        .onAppear {   }
        .onDisappear { self.model.cancel() }
        
        Button("Load Future", action: { self.model.loadImageWithFuture()})
    }
    
}


PlaygroundPage.current.setLiveView (CounterView(model: CounterViewModel()))
PlaygroundPage.current.needsIndefiniteExecution = true
