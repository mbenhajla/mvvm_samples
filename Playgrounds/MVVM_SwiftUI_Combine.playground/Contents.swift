import UIKit
import Foundation
import SwiftUI
import Combine
import PlaygroundSupport


var str = "Hello, MVVM SwiftUI"



// *************  ************* *************
// MVVM with SwiftUI and Combine and Local Data
// *************  ************* *************


// ************* Model & Repository*************
struct Quote:Identifiable, Decodable{
    var id :String
    var category:String
    var quoteText:String
    var author:String
    var thumbnail:String
    var webLink:String
}

enum DataError:Error{
    case httpError(error:Error)
    case parseError(errText:String, error:Error)
    case noData
    case unknown
}


import SwiftUI
import Combine

 protocol QuotesRepoProtocol_Combine {
     func loadQuotes() -> AnyPublisher<[Quote], Error>
 }
 


struct QuotesRepoService_Combine:QuotesRepoProtocol_Combine{
   
    private let readQueue = DispatchQueue(label: "ReadQueue", qos: .default, attributes: .concurrent)

    func createFuture () -> AnyPublisher<[Quote], Error>{
         return Deferred {
                Future { promise in
                               guard let url = Bundle.main.url(forResource: "quotesV1", withExtension: "json"),
                                 let data = try? Data(contentsOf: url),
                                 let quotes = try? JSONDecoder().decode([Quote].self, from: data)
                                 else {
                                        promise(.failure(DataError.unknown))
                                        return
                                    }
                             promise(.success(quotes))
                           }
                       //.receive(on: RunLoop.main)
                      
         }.eraseToAnyPublisher()
    }
    
    
    func loadQuotes() -> AnyPublisher<[Quote], Error>{
        let future = createFuture()  // nothing happens yet
        
        // the Future executes because it has a subscriber
        return future.subscribe(on: readQueue)
                        .receive(on: RunLoop.main)
                       .eraseToAnyPublisher()
    }
    
    
}


// ************* View Model *************
class QuotesViewModel_Combine:ObservableObject{
    
    @Published var quotes:[Quote]?
    
    var quoteService: QuotesRepoProtocol_Combine?
    
    // MARK:- Subscribers
 
    private var subscriptions = Set<AnyCancellable>()
    
    init(service:QuotesRepoProtocol_Combine) {
        quoteService = service
    }
    func fetch(){
        quoteService?.loadQuotes().sink(receiveCompletion: {  completion in
            print("Completion: \(completion)")
        }, receiveValue: { result in
            print("received value")
            self.quotes = result
            
        }).store(in: &subscriptions)
    }
}

// ************* View Model XCTest *************




struct QuotesView:View{
    
    @ObservedObject var model:QuotesViewModel_Combine = QuotesViewModel_Combine(service: QuotesRepoService_Combine())
    
    var body: some View{
        NavigationView {
            List(model.quotes ?? [Quote](), id: \.id ){ quote in
                Text(quote.quoteText)
            }
        }.onAppear {
            model.fetch()
        }
       
    }
    
}

// can be tested in iOS Simulator 

//PlaygroundPage.current.setLiveView (QuotesView())
//PlaygroundPage.current.needsIndefiniteExecution = true
