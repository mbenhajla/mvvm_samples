//  Created by Mohamed Ben Hajla on 14.12.20.

import UIKit
import PlaygroundSupport



var str = "Hello, MVVM w, Combine"

// *************  *************
// MVVM with View Controllers and with Combine 
// *************  *************


// ************* Model & Repository*************
struct Quote:Identifiable, Decodable{
    var id :String
    var category:String
    var quoteText:String
    var author:String
    var thumbnail:String
    var webLink:String
}

enum DataError:Error{
    case httpError(error:Error)
    case parseError(errText:String, error:Error)
    case noData
    case unknown
}

// *************  *************
// MVVM with View Controllers and Combine ( <= iOS13)
// *************  *************

import SwiftUI
import Combine

 protocol QuotesRepoProtocol_Combine {
     func loadQuotes() -> AnyPublisher<[Quote], Error>
 }
 



struct QuotesRepoService_Combine:QuotesRepoProtocol_Combine{
   
    private let readQueue = DispatchQueue(label: "RQ", qos: .default, attributes: .concurrent)

    func createFuture () -> AnyPublisher<[Quote], Error>{
         return Deferred {
                Future { promise in
                               guard let url = Bundle.main.url(forResource: "quotesV1", withExtension: "json"),
                                 let data = try? Data(contentsOf: url),
                                 let quotes = try? JSONDecoder().decode([Quote].self, from: data)
                                 else {
                                        promise(.failure(DataError.unknown))
                                        return
                                    }
                             promise(.success(quotes))
                           }
         }.eraseToAnyPublisher()
    }
    
    
    func loadQuotes() -> AnyPublisher<[Quote], Error>{
        // define future
        let future = createFuture()
        
        //  execute future on subscribing
        return future.subscribe(on: readQueue)
                        .receive(on: RunLoop.main)
                       .eraseToAnyPublisher()
    }
    
    
}


// ************* View Model *************
class QuotesViewModel_Combine:ObservableObject{
    
    @Published var quotes:[Quote]?
    
    var quoteService: QuotesRepoProtocol_Combine?
    
    // MARK:- Subscribers
 
    private var subscriptions = Set<AnyCancellable>()
    
    init(service:QuotesRepoProtocol_Combine) {
        quoteService = service
    }
    func fetch(){
        quoteService?.loadQuotes().sink(receiveCompletion: {  completion in
            print("Completion: \(completion)")
        }, receiveValue: { result in
            print("received value")
            self.quotes = result
            
        }).store(in: &subscriptions)
    }
}

// ************* View Model XCTest *************


// ************* View *************

class ViewController_Combine:UIViewController, UITableViewDelegate, UITableViewDataSource{
   
    
    var viewModel:QuotesViewModel_Combine = QuotesViewModel_Combine(service: QuotesRepoService_Combine())
    
    var subscriptions = [AnyCancellable]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        let tableView = UITableView()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.frame = view.frame
        tableView.center = view.center
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableViewCell")
        view.addSubview(tableView)
     
        // !!! This is one way of many
        self.viewModel.$quotes
           .receive(on: DispatchQueue.main)
           .sink {  _ in
              tableView.reloadData()
           }
           .store(in: &subscriptions)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.fetch()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.quotes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath)
                
        let quote = self.viewModel.quotes?[indexPath.row]
        if let q = quote {
            cell.textLabel?.text = q.quoteText
        }
        
        return cell
    }
    
    
}



let viewController_Combine = ViewController_Combine()
PlaygroundPage.current.liveView = viewController_Combine
PlaygroundPage.current.needsIndefiniteExecution = true
