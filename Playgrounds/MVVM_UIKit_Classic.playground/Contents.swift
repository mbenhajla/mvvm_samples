//  Created by Mohamed Ben Hajla on 14.12.20.

import UIKit
import PlaygroundSupport



var str = "Hello, MVVM classic"

// *************  *************
// MVVM with View Controllers and without Combine ( <= iOS12)
// *************  *************


// ************* Model & Repository*************
struct Quote:Identifiable, Decodable{
    var id :String
    var category:String
    var quoteText:String
    var author:String
    var thumbnail:String
    var webLink:String
}

enum DataError:Error{
    case httpError(error:Error)
    case parseError(errText:String, error:Error)
    case noData
    case unknown
}

protocol QuotesRepoProtocol{
    
    func loadQuotes(complete:@escaping(_ success:Bool, _ quotes:[Quote]?, _ error:DataError?) ->())
}



struct QuotesRepoService:QuotesRepoProtocol{
    
    func loadQuotes(complete:@escaping(_ success:Bool, _ quotes:[Quote]?, _ error:DataError?) ->()) {
        if let path = Bundle.main.path(forResource: "quotesV1", ofType: "json"){
            if let data = try? Data(contentsOf: URL(fileURLWithPath: path)){
                do {
                    let decoder = JSONDecoder()
                    let quotes = try decoder.decode([Quote].self, from: data)
                    print("quotes:\(quotes)")
                    complete( true, quotes, nil )
                } catch  {
                    print("error:\(error)")
                    complete(false, nil, DataError.parseError(errText: "Parse error", error: error))
                }
            }
        }
    }
}


// ************* View Model *************
class QuotesViewModel{
    
    var quoteService: QuotesRepoProtocol?
    
    var quotes:[Quote]?{
          didSet {
            self.refreshView?()
          }
      }
    var refreshView: (()->())?
    
    init(service:QuotesRepoProtocol) {
        quoteService = service
    }
    func fetch(){
        quoteService?.loadQuotes(complete: { (success, result, err) in
            self.quotes = result
            
        })
    }
}

// ************* View Model XCTest *************


// ************* View *************

class ViewController:UIViewController, UITableViewDelegate, UITableViewDataSource{
   
    
    var viewModel:QuotesViewModel = QuotesViewModel(service: QuotesRepoService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        let tableView = UITableView()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.frame = view.frame
        tableView.center = view.center
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableViewCell")
        view.addSubview(tableView)
        
        // Set refresh Model Closure
        self.viewModel.refreshView = { () in
            tableView.reloadData()
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.fetch()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.quotes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath)
                
        let quote = self.viewModel.quotes?[indexPath.row]
        if let q = quote {
            cell.textLabel?.text = q.quoteText
        }
        
        return cell
    }
    
    
}



let viewController = ViewController()
PlaygroundPage.current.liveView = viewController
PlaygroundPage.current.needsIndefiniteExecution = true


